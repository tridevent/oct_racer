
// Estas variables permiten que la secuencia de comandos para accionar las ruedas del coche.
var FrontLeftWheel : WheelCollider;
var FrontRightWheel : WheelCollider;
var steeringStrength : float = 30;

/*Estas variables son para los engranajes, la matriz puede ver la lista de ratios. La secuencia de comandos 
utiliza las relaciones de transmisión definidos para determinar la cantidad de par para aplicar a las ruedas.*/
var GearRatio : float[];
var CurrentGear : int = 0;

/*Estas variables son sólo para aplicar torsión a las ruedas y el cambio de marchas. 
utilizando el RPM Max y Min Motor definido, el script puede determinar qué equipo de la 
coche tiene que estar adentro*/
var EngineTorque : float = 0;//600    Reaccion del auto
var MaxEngineRPM : float = 3000.0;
var MinEngineRPM : float = 1000.0;
private var EngineRPM : float = 0.0;
//------------------------------------------
public var Acelerar: GUITexture;
public var Frenar: GUITexture;
public var Izquierda: GUITexture;
public var Derecha: GUITexture;
private var AcelerarPulsado : boolean = false;
private var FrenarPulsado : boolean = false;
private var IzquierdaPulsado : boolean = false;
private var DerechaPulsado : boolean = false;
//-------------------------------------------
var velocidadAdelante:float;
var velocidadAtras:float;
var velocidadGiro:float;


function Start () {
	// Normalmente me altero el centro de masa para hacer el coche más estable. Me menos probabilidades de dar la vuelta de esta manera.
	rigidbody.centerOfMass += Vector3(0, -1.0, .15);
	//rigidbody.centerOfMass += Vector3(0,0,0);
}

function Update () {
	
	/*Esto es para limith la velocidad máxima del vehículo, ajustando la resistencia no es probablemente la mejor manera de hacerlo, 
pero es fácil, y que no interfiere con el procesamiento de la física.*/
	rigidbody.drag = rigidbody.velocity.magnitude / 250;
	
	// Calcule las RPM del motor sobre la base de la RPM promedio de las dos ruedas, y luego llamar a la función de engranaje del cambio
	EngineRPM = (FrontLeftWheel.rpm + FrontRightWheel.rpm)/2 * GearRatio[CurrentGear];
	ShiftGears();

	/*establece el tono de audio con el porcentaje de RPM el régimen máximo más uno, esto hace que el sonido se reproduzca 
hasta el doble es de paso, donde se puede llegar a caerse cuando se cambia de marcha.*/
	audio.pitch = Mathf.Abs(EngineRPM / MaxEngineRPM) + 1.0 ;
	// esta línea es sólo para asegurarse de que el terreno de juego no llega a un valor más alto que lo deseado.
	if ( audio.pitch > 2.0 ) {
		audio.pitch = 2.0;
	}

	/* finalmente, aplicar los valores a las ruedas. El par de torsión aplicado se divide por la marcha actual, y 
multiplicado por la variable de entrada del usuario.*/
	FrontLeftWheel.motorTorque = EngineTorque / GearRatio[CurrentGear] * Input.GetAxis("Vertical");
	FrontRightWheel.motorTorque = EngineTorque / GearRatio[CurrentGear] * Input.GetAxis("Vertical");
	
		
	// el ángulo de dirección es un valor arbitrario multiplicado por la entrada de usuario.
	FrontLeftWheel.steerAngle = steeringStrength * Input.GetAxis("Horizontal");
	FrontRightWheel.steerAngle = steeringStrength * Input.GetAxis("Horizontal");
	//print(Input.GetAxis("Horizontal"));
	//--------------------------------------------------------------------------------
	if(Input.touchCount > 0)
	    {
	        for (var i = 0; i < Input.touchCount; ++i) 
	        {   
	            if(Acelerar.HitTest(Input.GetTouch(i).position))
	            {        
	                AcelerarPulsado = true;                         	           
	            }
	            if(Frenar.HitTest(Input.GetTouch(i).position))
	            {        
	                FrenarPulsado = true;                         	           
	            }
	            if(Izquierda.HitTest(Input.GetTouch(i).position))
	            {        
	                IzquierdaPulsado = true;                         	           
	            }
	            if(Derecha.HitTest(Input.GetTouch(i).position))
	            {        
	                DerechaPulsado = true;                         	           
	            }
	        } 
	    }
	    //---------------------------------------- 
		if (Input.GetMouseButton(0))
		{
	    	if (Acelerar.HitTest(Input.mousePosition))
	    	{
	    	AcelerarPulsado = true;
	    	}
	    	if (Frenar.HitTest(Input.mousePosition))
	    	{
	    	FrenarPulsado = true;
	    	}
	    	if (Izquierda.HitTest(Input.mousePosition))
	    	{
	    	IzquierdaPulsado = true;
	    	}
	    	if (Derecha.HitTest(Input.mousePosition))
	    	{
	    	DerechaPulsado = true;
	    	}
	    }
	    //----------------------------------------------------------
	    if(AcelerarPulsado)
	    {
		FrontRightWheel.motorTorque = EngineTorque / GearRatio[CurrentGear] *velocidadAdelante;
		FrontRightWheel.motorTorque = EngineTorque / GearRatio[CurrentGear] *velocidadAdelante;
		//FrontLeftWheel.steerAngle = 0;	
		//FrontRightWheel.steerAngle = -3;
	    }
	    
	    if(FrenarPulsado)
	    {
		FrontLeftWheel.motorTorque = EngineTorque / GearRatio[CurrentGear]* velocidadAtras;
		FrontRightWheel.motorTorque =EngineTorque / GearRatio[CurrentGear] * velocidadAtras;
		//FrontLeftWheel.steerAngle = -3;	
		//FrontRightWheel.steerAngle = 0;
	    } 
	    if(IzquierdaPulsado)
	    {
		//FrontLeftWheel.motorTorque = -400;
		//FrontRightWheel.motorTorque = EngineTorque / GearRatio[CurrentGear] * 0;
		FrontLeftWheel.steerAngle =steeringStrength *velocidadGiro*(-1);	
		FrontRightWheel.steerAngle =steeringStrength *velocidadGiro*(-1);
	    } 
	    if(DerechaPulsado)
	    {
		//FrontLeftWheel.motorTorque = -400;
		//FrontRightWheel.motorTorque = EngineTorque / GearRatio[CurrentGear] * 0;
		FrontLeftWheel.steerAngle = steeringStrength *velocidadGiro;	
		FrontRightWheel.steerAngle =steeringStrength *velocidadGiro;
	    }           		    	
	    AcelerarPulsado = false;
	    FrenarPulsado = false;
	    IzquierdaPulsado = false;
	    DerechaPulsado = false;
	  //----------------------------------------------------------------------------------
}

function ShiftGears() {
	/*este funciton cambia las marchas de la vehcile, se realiza un bucle a través de todos los engranajes, comprobando lo que hará 
las RPM del motor caiga dentro del intervalo deseado. La transmisión se establece a continuación, a este valor "apropiado".*/
	if ( EngineRPM >= MaxEngineRPM ) {
		var AppropriateGear : int = CurrentGear;
		
		for ( var i = 0; i < GearRatio.length; i ++ ) {
			if ( FrontLeftWheel.rpm * GearRatio[i] < MaxEngineRPM ) {
				AppropriateGear = i;
				break;
			}
		}
		
		CurrentGear = AppropriateGear;
	}
	
	if ( EngineRPM <= MinEngineRPM ) {
		AppropriateGear = CurrentGear;
		
		for ( var j = GearRatio.length-1; j >= 0; j -- ) {
			if ( FrontLeftWheel.rpm * GearRatio[j] > MinEngineRPM ) {
				AppropriateGear = j;
				break;
			}
		}
		
		CurrentGear = AppropriateGear;
	}
}